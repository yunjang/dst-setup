## Don't Starve Together Setup

Server setup is mostly straight forward.

- Download and install `Don't Starve Together` on Steam
- Subscribe to all the mods the server is using -- which can be obtained below:
  - [Access the Subscription List Here](https://steamcommunity.com/id/nullbahamut/myworkshopfiles/?appid=322330&browsefilter=myfavorites&browsesort=myfavorites&p=1)
  - The mods subscription list will contain a few client-side mods as well, I recommend enabling these for quality of life purposes
  - Steam might be dumb and open this in your preferred browser -- if so, login and subscribe or try to access that page from Steam browser itself
- Once subscribed to the mods, launch the game
- Press `Play` > `Browse Games`
- Read below for Server Information

## Server Information
Server Name: Doge Eats Together!

Password: Just message me.